#pragma once

#define BLOCK_SIZE 1024

struct PacketInfo
{
	int starBites;
	int type;
	int size;
	int offset;
	int count;
	int serialnumber; 
	int stopBits;
};

struct ListInfo
{
	int offset;		
	int size;
};

class Session
{
	public:
		Session(SOCKET clientSocket);
		~Session(void);

		Session* _parent;
		Session* _child;

		void TransferProc();
	
	private:
		SOCKET _clientSocket;
		char _recvbuf[BLOCK_SIZE];
		char _sendbuf[BLOCK_SIZE];
		PacketInfo _response; 
		char** _musicNames;
		int _musicCount;
		int _musicIndex;
		int _fileSize;

		void SetSendBuf(int type, int size = 0);
		void GetFiles();
		DWORD GetSize();
};

