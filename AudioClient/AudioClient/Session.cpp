#include <stdio.h>
#include "Session.h"
#include <mmreg.h>
#include "Data.h"
#include "DataQueue.h"
#include <iostream>
#include <string>

using namespace std;

DataQueue dataQueue;

void gSendProc(void* param)
{
	Session* session = (Session*)param;
	session->SendProc();
}

void gRecvProc(void* param)
{
	Session* session = (Session*)param;
	session->RecvProc();
}

void gKeyProc(void* param)
{
	Session* session = (Session*)param;
	session->KeyProc();
}

Session::Session(SOCKET clientSocket)
{
	_alive = 1;
	_clientSocket = clientSocket;
	_musicNames = NULL;
	_musicIndex = 0;
	_musicCount = 0;
	_musicOffset = 0;
	_keyStatus = 1;
	_recvOffset = 0;
	_recvSize = 0;
	_fileSize = 0;
	_fileOffset = 0;
	_sendStop = false;
	_request.starBites = 0x12345678;
	_request.type = 0;
	_request.size = sizeof(PacketInfo);
	_request.offset = 0;
	_request.count = 0;
	_request.serialnumber = 0;
	_request.stopBits = 0x87654321;

	_mutex = CreateMutex(NULL, FALSE, NULL);    
    if (_mutex == NULL) 
    {
        cout << "CreateMutex error: %d\n" << GetLastError() << endl;
		_alive = 0;
        return;
    }

	CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)gSendProc, this, 0, NULL);
	CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)gRecvProc, this, 0, NULL);
	CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)gKeyProc, this, 0, NULL);
}

Session::~Session(void)
{
	if(_clientSocket != INVALID_SOCKET)
	{
		closesocket(_clientSocket);
	}
	if(_musicNames != NULL) 
	{
		for(int i = 0; i < _musicCount; i++) 
		{
			if(_musicNames[i] != NULL) delete [] _musicNames[i];
		}
	}
}

void Session::KeyProc()
{
	char userChoice[32];
	do
	{
		if (_keyStatus == 0) {
			Sleep(1000);
		}
		else if (_keyStatus == 1)
		{
			cout << "\n********************************************\n";
			cout << "Press L to fetch music list from server. \n";
			cout << "Press song number to hear it. \n";
			cout << "Q to exit\n";
			cout << "********************************************\n";
			cout << "Your choice: " << endl;
			cin >> userChoice;

			if (userChoice[0] == 'l' || userChoice[0] == 'L')
			{
				SetSendBuf(1);
				_keyStatus = 0;
				cout << "\nFetching music list from server...\n";
			}
			else if (userChoice[0] == 'q' || userChoice[0] == 'Q')
			{
				_alive = -1;
				break;
			}
			else
			{
				int number = atoi(userChoice);
				if (number > 0 && number <= _musicCount)
				{
					_musicIndex = number - 1;
					SetSendBuf(3);
					_keyStatus = 0;
					cout << "\nInitializing song...\n\n";
				}
			}
		}
		else if (_keyStatus == 2)
		{
			cout << "\nP for play song.\n";
			cout << "S to stop song.\n";
			cout << "A to pause song.\n";
			cout << "X to get back to menu.\n" << endl;
			do
			{
				cout << "Your choice: " << endl;
				cin >> userChoice;
				if (userChoice[0] == 'p' || userChoice[0] == 'P')
				{
					if (!_audio.IsAvailable())
					{
						SetSendBuf(3);
						_keyStatus = 0;
						while (!_audio.IsAvailable()) Sleep(500);
					}
					_audio.Play();
				}
				else if (userChoice[0] == 's' || userChoice[0] == 'S')
				{
					_audio.Stop();
					SetSendBuf(3);
					_keyStatus = 0;
					break;
				}
				else if (userChoice[0] == 'a' || userChoice[0] == 'A')
				{
					_audio.Pause();
				}
				else if (userChoice[0] == 'x' || userChoice[0] == 'X')
				{
					_audio.Stop();
					_keyStatus = 1;
					break;
				}
			} while (true);
		}
	} while (true);
}

void Session::SendData()
{
	DWORD dwWaitResult = WaitForSingleObject(_mutex, INFINITE);       

	int len = send(_clientSocket, _sendbuf, sizeof(PacketInfo), 0 );
	if (len == SOCKET_ERROR) {
		_alive = 0;
	}

	ReleaseMutex(_mutex); 
}

void Session::SetSendBuf(int type, int serialnumber)
{
	_request.type = type;
	if (serialnumber != -1) {
		_request.serialnumber = serialnumber;
	}

	if(_request.type == 2) _request.offset = _musicOffset;
	else if(_request.type == 3) _request.offset = _musicIndex;
	else if(_request.type == 4) _request.offset = _fileOffset;

	DWORD dwWaitResult = WaitForSingleObject(_mutex, INFINITE);        
	
	memcpy(_sendbuf, &_request, sizeof(PacketInfo));
	
	ReleaseMutex(_mutex); 
}

void Session::SendProc()
{
	while(_alive == 1)
	{
		if(_request.type == 0)
		{
			Sleep(500);
		}

		Sleep(2); 
		if(_sendStop) continue;
		if(_request.type == 4 && !dataQueue.IsExistFree()) continue;

		SendData();
	}
}

void Session::RecvProc()
{
	while(_alive == 1)
	{
		try
		{
			_sendStop = false;
			int len = recv(_clientSocket, _recvbuf, BLOCK_SIZE, 0);
			_sendStop = true;

			if(len <= 0)
			{
				printf_s("\nRecv failed with error: %d\n", WSAGetLastError());
				_alive = 0;
				break;
			}

			int offset = 0;
			if(_recvSize == 0)
			{
				if(len < sizeof(PacketInfo)) continue;

				PacketInfo* response = (PacketInfo*)_recvbuf;
				if(response->starBites != 0x12345678) continue;
				if(response->stopBits != 0x87654321) continue;

				_recvSize = response->size;

				memcpy(_recvData, _recvbuf, sizeof(PacketInfo));
				_recvOffset = sizeof(PacketInfo);
				offset = sizeof(PacketInfo);
			}

			int remain = min(len - offset, _recvSize - _recvOffset);
			if(remain > 0)
			{
				memcpy(_recvData + _recvOffset, _recvbuf + offset, remain);
				_recvOffset += remain;
				offset += remain;
			}

			if(_recvOffset >= _recvSize)
			{
				ParseData();
				_recvOffset = 0;
				_recvSize = 0;
			}
		}
		catch(...)
		{
			cout << "\nRecv failed with error: %d\n" << WSAGetLastError() << endl;
			_alive = 0;
			break;
		}
	}
}

void Session::ParseData()
{
	PacketInfo* response = (PacketInfo*)_recvData;
	if(response->type == 1)
	{
		if(_musicNames != NULL) 
		{
			for(int i = 0; i < _musicCount; i++) 
			{
				if(_musicNames[i] != NULL) delete [] _musicNames[i];
			}
		}
		_musicCount = response->count;
		_musicNames = (char**)(new char*[_musicCount]);
		for(int i = 0; i < _musicCount; i++) _musicNames[i] = NULL;
		_musicOffset = 0;

		SetSendBuf(2, response->serialnumber);
	}
	else if(response->type == 2)
	{
		if(_musicNames == NULL)
		{
			SetSendBuf(1, response->serialnumber);
			return;
		}

		if(_musicOffset != _request.offset) 
		{
			SetSendBuf(2, response->serialnumber);
			return;
		}

		int offset = sizeof(PacketInfo);
		do
		{
			ListInfo* listInfo = (ListInfo*)(_recvData + offset);
			if (listInfo->offset != _musicOffset) break;
			offset += sizeof(ListInfo);

			if (_recvSize - offset < listInfo->size) break;

			_musicNames[_musicOffset] = new char[listInfo->size];
			memcpy(_musicNames[_musicOffset], _recvData + offset, listInfo->size);
			cout << "\n" << _musicOffset + 1 << ")" << _musicNames[_musicOffset] << endl;
			_musicOffset++;
			offset += listInfo->size;
		} while (offset < _recvSize && _musicOffset < _musicCount);

		if(_musicOffset < _musicCount)
		{
			SetSendBuf(2, response->serialnumber);
			return;
		}

		_keyStatus = 1;
		SetSendBuf(0, response->serialnumber);
	}
	else if(response->type == 3)
	{
		_fileSize = response->count;
		if(_fileSize == -1) 
		{
			cout << "[%s couldn't be read\n" << _musicNames[_musicIndex] << endl;
			_keyStatus = 1;
			SetSendBuf(0, response->serialnumber);
			return;
		}
		else if(_fileSize < 1024) printf_s("\n%s Filesize: %d Bytes\n" , _musicNames[_musicIndex] , _fileSize);
		else printf_s("\n%s Filesize: %d KB\n", _musicNames[_musicIndex] , _fileSize / 1024);

		_audio.Start();
		_fileOffset = 0;
		_keyStatus = 2;
		SetSendBuf(4, response->serialnumber);
	}
	else if(response->type == 4)
	{
		if(_audio.IsAvailable()) _keyStatus = 2;

		_blockSize = _recvSize - sizeof(PacketInfo);
		if(_fileOffset != response->offset) 
		{
			SetSendBuf(4, response->serialnumber);
			return;
		}

		dataQueue.AddQueue(_recvData + sizeof(PacketInfo), _blockSize);
		_fileOffset += _blockSize;

		if(_fileOffset < _fileSize)
		{
			SetSendBuf(4, response->serialnumber);
			return;
		}

		_audio.Done();
		SetSendBuf(0, response->serialnumber);
	}
}