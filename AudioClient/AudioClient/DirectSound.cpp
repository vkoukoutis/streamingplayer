#include "DirectSound.h"
#include "Data.h"
#include "DataQueue.h"

#pragma comment(lib, "dsound.lib")
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "winmm.lib")

extern DataQueue dataQueue;

void gPlayProc(void* param)
{
	DirectSound* audio = (DirectSound*)param;
	audio->PlayProc();
}

DirectSound::DirectSound(void)
{
	directSound = NULL;
	primaryBuffer = NULL;
	secondaryBuffer = NULL;
	m_hThread = NULL;
	m_dwOffset = 0;
	m_dwSize = 0;
	m_bAvailable = false;

	Initialize();
}

DirectSound::~DirectSound(void)
{
	Stop();
	Shutdown();
}

bool DirectSound::Initialize()
{
	HRESULT result;
	DSBUFFERDESC bufferDesc;
	WAVEFORMATEX waveFormat;

	result = DirectSoundCreate8(NULL, &directSound, NULL);
	if (FAILED(result))
	{
		return false;
	}

	result = directSound->SetCooperativeLevel(GetDesktopWindow(), DSSCL_PRIORITY);
	if (FAILED(result))
	{
		return false;
	}

	bufferDesc.dwSize = sizeof(DSBUFFERDESC);
	bufferDesc.dwFlags = DSBCAPS_PRIMARYBUFFER | DSBCAPS_CTRLVOLUME;
	bufferDesc.dwBufferBytes = 0;
	bufferDesc.dwReserved = 0;
	bufferDesc.lpwfxFormat = NULL;
	bufferDesc.guid3DAlgorithm = GUID_NULL;

	result = directSound->CreateSoundBuffer(&bufferDesc, &primaryBuffer, NULL);
	if (FAILED(result))
	{
		return false;
	}

	waveFormat.wFormatTag = WAVE_FORMAT_PCM;
	waveFormat.nSamplesPerSec = 44100;
	waveFormat.wBitsPerSample = 16;
	waveFormat.nChannels = 2;
	waveFormat.nBlockAlign = (waveFormat.wBitsPerSample / 8) * waveFormat.nChannels;
	waveFormat.nAvgBytesPerSec = waveFormat.nSamplesPerSec * waveFormat.nBlockAlign;
	waveFormat.cbSize = 0;

	result = primaryBuffer->SetFormat(&waveFormat);
	if (FAILED(result))
	{
		return false;
	}
	return true;
}

DWORD DirectSound::ReadData(void* data, DWORD size)
{
	int offset = 0;
	while(offset < size)
	{
		int len = min(m_dwSize - m_dwOffset, size - offset);
		if(len > 0)
		{
			memcpy((char*)data + offset, m_Data + m_dwOffset, len);
			m_dwOffset += len;
			offset += len;
		}
		else
		{
			while(true) 
			{
				if(dataQueue.removeQueue(m_Data, m_dwSize))
				{
					m_dwOffset = 0;
					break;
				}
				else if(m_bDone)
				{
					m_bEmpty = true; 
					memset((char*)data + offset, 0, size - offset);
					m_dwSize = 0;
					m_dwOffset = 0;
					return size;
				}
				Sleep(2);
			}
		}
	}
	return size;
}

void DirectSound::MoveData(DWORD offset, DWORD start)
{
	if (start == FILE_BEGIN) {
		m_dwOffset = offset;
	}
	else if (start == FILE_CURRENT) {
		m_dwOffset += offset;
	}
}

bool DirectSound::FindChunk(FOURCC fourcc, DWORD& chunkSize, DWORD& chunkDataPosition)
{
	DWORD chunkType;
	DWORD chunkDataSize;
	DWORD riffDataSize = 0;
	DWORD fileType;
	DWORD bytesRead = 0;
	DWORD offset = 0;

	MoveData(0, FILE_BEGIN);
	do 
	{
		bytesRead = ReadData(&chunkType, sizeof(DWORD));
		bytesRead = ReadData(&chunkDataSize, sizeof(DWORD));
		switch (chunkType)
		{
		case fourccRIFF:
			riffDataSize = chunkDataSize;
			chunkDataSize = 4;
			bytesRead = ReadData(&fileType, sizeof(DWORD));
			break;
		case fourccDATA:
			break;

		default:
			MoveData(chunkDataSize, FILE_CURRENT);
		}

		offset += sizeof(DWORD) * 2;
		if (chunkType == fourcc)
		{
			chunkSize = chunkDataSize;
			chunkDataPosition = offset;
			return true;
		}

		offset += chunkDataSize;
		if (bytesRead >= riffDataSize)
		{
			return false;
		}
	} while (true);

	return true;
}

bool DirectSound::ReadChunkData(void* buffer, DWORD buffersize, DWORD bufferoffset)
{
	MoveData(bufferoffset, FILE_BEGIN);
	ReadData(buffer, buffersize);
	return true;
}

void DirectSound::PlayProc()
{
	WAVEFORMATEXTENSIBLE wfx = { 0 };
	WAVEFORMATEX waveFormat;
	DWORD chunkSize;
	DWORD chunkPosition;
	DWORD filetype;
	DSBUFFERDESC bufferDesc;
	IDirectSoundBuffer * tempBuffer;

	FindChunk(fourccRIFF, chunkSize, chunkPosition);
	ReadChunkData(&filetype, sizeof(DWORD), chunkPosition);
	if (filetype != fourccWAVE) return;

	FindChunk(fourccFMT, chunkSize, chunkPosition);
	ReadChunkData(&wfx, chunkSize, chunkPosition);
	FindChunk(fourccDATA, chunkSize, chunkPosition);

	waveFormat.wFormatTag =  wfx.Format.wFormatTag;
	waveFormat.nSamplesPerSec = wfx.Format.nSamplesPerSec;
	waveFormat.wBitsPerSample = wfx.Format.wBitsPerSample;
	waveFormat.nChannels = wfx.Format.nChannels;
	waveFormat.nBlockAlign = wfx.Format.nBlockAlign;
	waveFormat.nAvgBytesPerSec = wfx.Format.nAvgBytesPerSec;
	waveFormat.cbSize = 0;

	bufferDesc.dwSize = sizeof(DSBUFFERDESC);
	bufferDesc.dwFlags = DSBCAPS_CTRLVOLUME | DSBCAPS_GLOBALFOCUS | DSBCAPS_CTRLPOSITIONNOTIFY;
	bufferDesc.dwBufferBytes = 4 * waveFormat.nAvgBytesPerSec;
	bufferDesc.dwReserved = 0;
	bufferDesc.lpwfxFormat = &waveFormat;
	bufferDesc.guid3DAlgorithm = GUID_NULL;

	HRESULT result = directSound->CreateSoundBuffer(&bufferDesc, &tempBuffer, NULL);
	if (FAILED(result)) return;

	result = tempBuffer->QueryInterface(IID_IDirectSoundBuffer8, (void**)&secondaryBuffer);
	if (FAILED(result)) return;

	tempBuffer->Release();
	PlayWave(waveFormat);
}

void DirectSound::PlayWave(WAVEFORMATEX waveFormat)
{
	HRESULT result;
	unsigned char*	bufferPtr1;
	unsigned long   bufferSize1;
	unsigned char*	bufferPtr2;
	unsigned long   bufferSize2;

	DWORD soundBytesOutput = 0;
	bool fillFirstHalf = true;
	LPDIRECTSOUNDNOTIFY8 directSoundNotify;
	DSBPOSITIONNOTIFY positionNotify[2];

	result = secondaryBuffer->SetCurrentPosition(0);
	if (FAILED(result)) return;

	result = secondaryBuffer->SetVolume(DSBVOLUME_MAX);
	if (FAILED(result)) return;
 
	HANDLE playEventHandles[2];
	playEventHandles[0] = CreateEvent(NULL, FALSE, FALSE, NULL);
	playEventHandles[1] = CreateEvent(NULL, FALSE, FALSE, NULL);

	result = secondaryBuffer->QueryInterface(IID_IDirectSoundNotify8, (LPVOID*)&directSoundNotify);
	if (FAILED(result)) return;

	positionNotify[0].dwOffset = waveFormat.nAvgBytesPerSec / 2;
	positionNotify[0].hEventNotify = playEventHandles[0];
	positionNotify[1].dwOffset = waveFormat.nAvgBytesPerSec * 3 / 2;
	positionNotify[1].hEventNotify = playEventHandles[1];
	directSoundNotify->SetNotificationPositions(2, positionNotify);

	result = secondaryBuffer->Lock(0, waveFormat.nAvgBytesPerSec * 2, (void**)&bufferPtr1, (DWORD*)&bufferSize1, (void**)&bufferPtr2, (DWORD*)&bufferSize2, 0);
	if (FAILED(result)) return;

	ReadData(bufferPtr1, bufferSize1);
	if (bufferPtr2 != NULL) ReadData(bufferPtr2, bufferSize2);

	result = secondaryBuffer->Unlock((void*)bufferPtr1, bufferSize1, (void*)bufferPtr2, bufferSize2);
	if (FAILED(result)) return;

    m_bAvailable = true;

	DWORD dwRetSamples = 0, dwRetBytes = 0;

	do
	{
		result = WaitForMultipleObjects(2, playEventHandles, FALSE, INFINITE);
		if (WAIT_OBJECT_0 == result)
		{
			HRESULT hr = secondaryBuffer->Lock(2 * waveFormat.nAvgBytesPerSec, 4 * waveFormat.nAvgBytesPerSec,
				(void**)&bufferPtr1, (DWORD*)&bufferSize1, (void**)&bufferPtr2, (DWORD*)&bufferSize2, 0);
			if (FAILED(hr)) return;
		}
		else if (WAIT_OBJECT_0 + 1 == result)
		{
			HRESULT hr = secondaryBuffer->Lock(0, 2 * waveFormat.nAvgBytesPerSec, (void**)&bufferPtr1, (DWORD*)&bufferSize1,
				(void**)&bufferPtr2, (DWORD*)&bufferSize2, 0);
			if (FAILED(hr)) return;
		}
		else return;

		dwRetBytes = dwRetSamples * waveFormat.nBlockAlign;

		if (dwRetSamples < waveFormat.nSamplesPerSec)
		{
			memset(bufferPtr1 + dwRetBytes, 0, waveFormat.nAvgBytesPerSec - dwRetBytes);
		}

		ReadData(bufferPtr1, bufferSize1);
		if (NULL != bufferPtr2)
		{
			ReadData(bufferPtr2, bufferSize2);
		}

		//secondaryBuffer->Unlock(bufferPtr1, bufferSize1, bufferPtr2, bufferSize2);
		if (m_bEmpty)
		{
			Pause();
			break;
		}
	} while (true);

	directSoundNotify->Release();
	CloseHandle(playEventHandles[0]);
	CloseHandle(playEventHandles[1]);
	Stop();
}

bool DirectSound::IsAvailable()
{
	return m_bAvailable;
}
	
void DirectSound::Start()
{
	dataQueue.Clear();
	m_dwOffset = 0;
	m_dwSize = 0;
	m_bDone = false;
	m_bEmpty = false;

	m_hThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)gPlayProc, this, 0, NULL);
	if(m_hThread == NULL) return;
}

void DirectSound::Play()
{
	if (secondaryBuffer != NULL) {
		secondaryBuffer->Play(0, 0, DSBPLAY_LOOPING);
	}
}

void DirectSound::Pause()
{
	if (secondaryBuffer != NULL) {
		secondaryBuffer->Stop();
	}
}

void DirectSound::Done()
{
	m_bDone = true;
}

void DirectSound::Stop()
{
    m_bAvailable = false;

	Pause();

	if(m_hThread == NULL) return;
	if(::WaitForSingleObject(m_hThread, 1000) != WAIT_OBJECT_0)
	{
		m_hThread = NULL;
	}

	if (secondaryBuffer != NULL)
	{
		secondaryBuffer->Release();
		secondaryBuffer = NULL;
	}
}

void DirectSound::Shutdown()
{
	if (primaryBuffer != NULL)
	{
		primaryBuffer->Release();
		primaryBuffer = NULL;
	}

	if (directSound != NULL)
	{
		directSound->Release();
		directSound = NULL;
	}
}
