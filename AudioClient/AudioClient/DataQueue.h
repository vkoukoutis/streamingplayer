#pragma once

#include "Data.h"

class DataQueue
{
public:
	DataQueue();
	~DataQueue();

	void	AddQueue(char* data, int count);
	bool	removeQueue(char* data, int& count);
	bool	IsExistFree();
	void	Clear();

protected:
	Data*  m_pRoot;
	Data*  m_pLastNext;
	int	   m_iCount;
};
