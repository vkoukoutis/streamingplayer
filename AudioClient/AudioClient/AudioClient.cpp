#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <winsock2.h>
#include "stdio.h"
#include "Session.h"
#include <iostream>
#pragma comment (lib, "ws2_32.lib")

char ip[32];
int port = 9000;
SOCKET clientSocket = INVALID_SOCKET;

using namespace std;

bool SetServer()
{
	if(ip[0] == 0) 
	{
		cout << "Server IP Address: ";
		scanf_s("%s", ip);
	}
	else
	{
		printf_s("Press 'S' to set a valid IP Address or 'Q' to quit.\n");
		char key[32];
		scanf_s("%s", &key);
		if(key[0] == 's' || key[0] == 'S') 
		{
			printf_s("Server IP Address: ");
			scanf_s("%s", ip);
		}
		else if(key[0] == 'q' || key[0] == 'Q') return false;
	}
	return true;
}

bool Initialize()
{
	ip[0] = 0;
    WSADATA wsaData;
    int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (iResult != 0) 
	{
        printf_s("WSAStartup failed with error: %d\n", iResult);
        return false;
    }
	return true;
}

void Close()
{
	if(clientSocket != INVALID_SOCKET)
	{
		closesocket(clientSocket);
		clientSocket = INVALID_SOCKET;
	}
	WSACleanup();	
}

int main()
{
	if(!Initialize()) return -1;

	while (true)
	{
		if(!SetServer()) break;

		sockaddr_in sa;
		sa.sin_family = AF_INET;
		sa.sin_addr.s_addr = inet_addr(ip);
		sa.sin_port = htons(port);

		clientSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if(clientSocket == INVALID_SOCKET)
		{
			printf_s("socket failed with error: %d\n", WSAGetLastError());
			continue;
		}

		if (connect(clientSocket, (SOCKADDR*)&sa, sizeof (sa)) == SOCKET_ERROR) 
		{
			closesocket(clientSocket);
			clientSocket = INVALID_SOCKET;
			printf_s("\nInvalid IP Address.\n\n", WSAGetLastError());
			continue;
		}

		Session* session = new Session(clientSocket);

		while(session->_alive == 1) Sleep(500);
		int alive = session->_alive;
		delete session;
		if(alive == -1) break;
	}

	Close();
	return 0;
}

