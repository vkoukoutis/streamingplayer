#pragma once

#include "DirectSound.h"

struct PacketInfo
{
	int starBites;		
	int type;			
	int size;
	int offset;
	int count;
	int serialnumber; 
	int stopBits;	
};

struct ListInfo
{
	int offset;		
	int size;
};

class Session
{
	public:
		Session(SOCKET clientSocket);
		~Session(void);

		int _alive;
		void SendProc();
		void RecvProc();
		void KeyProc();

	private:
		HANDLE _mutex; 
		SOCKET _clientSocket;
		char _recvbuf[BLOCK_SIZE];
		char _recvData[BLOCK_SIZE];
		int _recvOffset;
		int _recvSize;
		char _sendbuf[BLOCK_SIZE];
		PacketInfo _request;
		char** _musicNames;
		int _musicIndex;
		int _musicCount;
		int _musicOffset;
		char _fileBlock[BLOCK_SIZE];
		int _blockSize;
		int _fileSize;
		int _fileOffset;
		int _keyStatus;
		bool _sendStop;
		DirectSound _audio;

		void SendData();
		void SetSendBuf(int type, int serialnumber = -1);
		void ParseData();
};

