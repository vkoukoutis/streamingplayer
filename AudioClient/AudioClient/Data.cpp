#include <windows.h>
#include "Data.h"

Data::Data(char* pData, int count)
{
	m_pData = new char[count];
	memcpy(m_pData, pData, count);
	m_iSize = count;
	m_pNext = NULL;
}

Data::~Data()
{
	delete [] m_pData;
}

void Data::Copy(char* pData, int& count)
{
	count = m_iSize;
	memcpy(pData, m_pData, count);
}
